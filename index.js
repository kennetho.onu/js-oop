//How do you create arrays in JS?
//By using an array literal. Example:

const students = ["John", "Jerry", "James", "Johnson"];


//How do you access the first character of an array?
//mapping. Example:

// let firstChar = students.map(function(student) {
//      return student[0]
// })

//let firstChar = students.slice(-1);

//How do you access the last character of an array?
//By using the slice method. Example:

//let lastChar = students.map(function(student) {
//	return student[students.length-1]
//})


//What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array.
//indexOf();

//What array method loops over all elements of an array, performing a user-defined function on each iteration?
//forEach();


//What array method creates a new array with elements obtained from a user-defined function?
//filter method


//What array method checks if all its elements satisfy a given condition?
//every()

//What array method checks if at least one of its elements satisfies a given condition?
//some()

//True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
//False

//True or False: array.slice() copies elements from original array and returns them as a new array.
//True

//Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.
//let name = "Ryan"

//function addToEnd() {
//	if () {

//	} else if() {

//	}else{

//	}
//}

//Create a function named addToStart that will add a passed in string to the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing.
//let girl = "Tess"

//function addToStart() {
//	if() {

//	} else if() {

//	}else{

//	}
//}

//Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.
//let female = "Jane"

//function elementChecker() {
// 	if() {

//	}else if() {

//	}else{

//	}
//}


/*
Create a function named checkAllStringsEnding that will accept a passed in array and a character. The function will do the ff:


if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
if every element in the array ends in the passed in character, return true. Otherwise return false.

Use the students array and the character "e" as arguments when testing.

function checkAllStringsEnding() {
	
}


*/



//Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements must be strings". Otherwise, return the sorted array. Use the students array to test.

/*


Create a function named startsWithCounter that will take in an array of strings and a single character. The function will do the ff:



if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
return the number of elements in the array that start with the character argument, must be case-insensitive

Use the students array and the character "J" as arguments when testing.
*/



//Create a function named randomPicker that will take in an array and output any one of its elements at random when invoked. Pass in the students array as an argument when testing.

function randomPicker() {
	let randomNum = Math.trunc(Math.random(students) * 4)
	console.log(students[randomNum])

}

